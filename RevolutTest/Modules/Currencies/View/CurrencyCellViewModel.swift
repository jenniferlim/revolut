//
//  CurrencyCellViewModel.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation

struct CurrencyCellViewModel {

    // MARK: - Properties

    let countryFlag: String
    let countryName: String
    let symbol: String
    let amount: String

    // MARK: - Init

    init(currency: Currency, value: Double? = nil) {
        self.countryFlag = currency.flag
        self.countryName = currency.name
        self.symbol = currency.rawValue

        if let value = value, value != 0 {
            self.amount = String(format: "%.2f", value)
        } else {
            self.amount = ""
        }
    }
}

extension CurrencyCellViewModel: Equatable {
    public static func == (lhs: CurrencyCellViewModel, rhs: CurrencyCellViewModel) -> Bool {
        return  lhs.countryFlag == rhs.countryFlag &&
                lhs.countryName == rhs.countryName &&
                lhs.symbol == rhs.symbol
    }

}

//
//  RevolutTestTests.swift
//  RevolutTestTests
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import XCTest
@testable import RevolutTest

class RevolutTestTests: XCTestCase {

	var presenter: CurrenciesListPresenter = CurrenciesListPresenter()
	var interactor: MockCurrenciesListInteractor = MockCurrenciesListInteractor()
	var view = MockCurrenciesListViewController()
	var router = CurrenciesListRouter()

    override func setUp() {
		super.setUp()

		self.presenter.interactor = interactor
		self.presenter.view = view
		self.presenter.router = router
		self.interactor.output = presenter
    }

	func testPresenterHasSucessfullyFetchedCurrenciesRate() {
		self.presenter.viewDidLoad()

		XCTAssertTrue(self.interactor.fetchLatestCurrenciesCalled)
	}

	func testDatasourceValues() {
		self.presenter.viewDidLoad()

		XCTAssertEqual(self.presenter.currenciesRate?.base, "EUR")
		XCTAssertNotNil(self.presenter.currenciesRate)
		XCTAssertEqual(self.presenter.currenciesRate?.rates.count, 32)
	}

	func testTextIsValid() {
        // Restrict non-numeric characters (except for decimals and backspaces)
		XCTAssertFalse(self.presenter.textIsValid("e"))
        XCTAssertFalse(self.presenter.textIsValid("2.e"))
        XCTAssertTrue(self.presenter.textIsValid(""))
        XCTAssertFalse(self.presenter.textIsValid("1.1.1"))

        // Restrict ridiculous amounts of money, defensively prevent overflows
		XCTAssertFalse(self.presenter.textIsValid("9999999999999"))
        XCTAssertTrue(self.presenter.textIsValid("999999999"))

        // Only allow two digits after an initial decimal with no leading zero
        XCTAssertTrue(self.presenter.textIsValid(".12"))
        XCTAssertFalse(self.presenter.textIsValid(".123"))

        // 2 components are dollars and cents
        // Prevent more than 2 decimal digits from being entered
        XCTAssertTrue(self.presenter.textIsValid("1.12"))
        XCTAssertFalse(self.presenter.textIsValid("1.123"))

        // Check some more classic working cases
		XCTAssertTrue(self.presenter.textIsValid("12."))
        XCTAssertTrue(self.presenter.textIsValid("12.1"))
        XCTAssertTrue(self.presenter.textIsValid("1"))
	}
}

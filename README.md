## Readme

This project is done using the VIPER architecture. To launch the app, open the .xcodeproj, then run on simulator: A list of currencies rate should appear. Select a currency to make it the base currency: The currency should move to the top, type `1` for instance, all the rates should get updated.

The test coverage on the project is around 73%.

I built this project in a way that would be easy for a large team to work on it, easy to maintain and scale.

My guess was that i should spend some time building a good architecture rather than building the project very quickly but not easy to maintain.

I spend some time working on:
- The network layer
- I tried to cover different edge cases on what values can the amount textfield accept
- Unit tests

Regarding the time management, i spent my whole day working on this project (roughly 10 hours). I was able to get a nice VIPER architecture done with a solid networking layer done.


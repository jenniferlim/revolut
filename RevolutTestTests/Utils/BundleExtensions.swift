//
//  BundleExtensions.swift
//  RevolutTestTests
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation

class RevolutTests {}

extension Bundle {

	public static var revolutTests: Bundle {
		return Bundle(for: RevolutTests.self)
	}
}

//
//  AmountTextField.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation
import UIKit

class AmountTextField: UITextField {

    var isEditable: Bool = false

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)

        guard view != self || isEditable else {
            return nil
        }

        return view
    }
}

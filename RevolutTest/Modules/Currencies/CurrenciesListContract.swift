//
//  CurrenciesListContract.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit

protocol CurrenciesListView: class {
    var presenter: CurrenciesListPresentation! { get set }
    
    func reload(currencyViewModels: [CurrencyCellViewModel], baseCurrencyViewModel: CurrencyCellViewModel)
    func moveItem(from: IndexPath, to: IndexPath, with viewModels: [CurrencyCellViewModel])
    func textFieldShouldBecomeFirstResponder(at indexPath: IndexPath)
}

protocol CurrenciesListPresentation: class {
    var view: CurrenciesListView? { get set }
    var interactor: CurrenciesListInteractor! { get set }
    var router: CurrenciesListWireframe! { get set }
    
    func viewDidLoad()
    func didSelectItem(at indexPath: IndexPath, with currencyAmount: String)
    func didChange(baseCurrencyAmount: String)
	func textIsValid(_ text: String) -> Bool
}

protocol CurrenciesListInteractorOutput: class {
    func currenciesRateFetched(currenciesRate: CurrenciesRate)
    func currenciesRateFetchFailed()
}

protocol CurrenciesListUseCase: class {
    var output: CurrenciesListInteractorOutput! { get set }
    func fetchLatestCurrencies(currency: String)
}

protocol CurrenciesListWireframe: class {
    var viewController: UIViewController? { get set }
    static func start() -> UIViewController
}

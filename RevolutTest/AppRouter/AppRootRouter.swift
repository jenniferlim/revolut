//
//  AppRootRouter.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit

class AppRootRouter: AppRootWireframe {
    func start(in window: UIWindow) {
        window.makeKeyAndVisible()
        window.rootViewController = CurrenciesListRouter.start()
    }
}

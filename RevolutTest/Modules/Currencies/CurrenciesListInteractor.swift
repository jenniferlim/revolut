//
//  CurrenciesListInteractor.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation

class CurrenciesListInteractor: CurrenciesListUseCase {
    
    var output: CurrenciesListInteractorOutput!
    let client = Client()
    
    func fetchLatestCurrencies(currency: String) {

        let parameters = ["base": currency]
        let path = Client.Route.latest.rawValue

        self.client.performRequest(path: path, parameters: parameters) { [weak self] (result: Result<CurrenciesRate>) in
            switch result {

            case .success(let currenciesRate):
                self?.output.currenciesRateFetched(currenciesRate: currenciesRate)
            case .error:
                self?.output.currenciesRateFetchFailed()
            }
        }
    }
}

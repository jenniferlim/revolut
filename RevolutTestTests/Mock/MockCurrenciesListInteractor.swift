//
//  MockCurrenciesListInteractor.swift
//  RevolutTestTests
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit
@testable import RevolutTest

class MockCurrenciesListInteractor: CurrenciesListInteractor {

	var fetchLatestCurrenciesCalled: Bool = false

	override func fetchLatestCurrencies(currency: String) {

		self.fetchLatestCurrenciesCalled = true

		let currenciesRate: CurrenciesRate = json(filename: "Latest")
		self.output.currenciesRateFetched(currenciesRate: currenciesRate)
	}
}

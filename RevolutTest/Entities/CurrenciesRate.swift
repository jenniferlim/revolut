//
//  CurrenciesRate.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation

struct CurrencyRate: Codable {
    let currency: Currency
    let rate: Double
}

struct CurrenciesRate: Codable {
    let base: String
    let date: String
    var rates: [CurrencyRate]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.base = try container.decode(String.self, forKey: .base)
        self.date = try container.decode(String.self, forKey: .date)
        
        let ratesDico = try container.decode(Dictionary<String, Double>.self, forKey: .rates)
        
        var list = [CurrencyRate]()
        for (key, value) in ratesDico {
            list.append(CurrencyRate(currency: Currency(rawValue: key)!, rate: value))
        }

        self.rates = list.sorted() { $0.currency.rawValue < $1.currency.rawValue }
    }
}

//
//  CurrencyCell.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit

class CurrencyCell: UICollectionViewCell {

    private struct Constants {
        static let CorneRadius: CGFloat = 6.0
        static let Margin: CGFloat = 16.0
    }

    // MARK: - Properties

    let amountTextField: AmountTextField = {
        let textField = AmountTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = Font.AmountText
        textField.textAlignment = .right
        textField.placeholder = "0"
        textField.keyboardType = .decimalPad
        return textField
    }()

    private lazy var verticalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, countryLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()

    private let countryFlag: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Font.Flag
        label.setContentHuggingPriority(.required, for: .horizontal)
        return label
    }()

    private let countryLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Font.Text
        label.textColor = UIColor.gray
        return label
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Font.Title
        return label
    }()

    private lazy var horizontalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [countryFlag, verticalStackView, amountTextField])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = Constants.Margin
        return stackView
    }()

    var viewModel: CurrencyCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            self.countryFlag.text = viewModel.countryFlag
            self.countryLabel.text = viewModel.countryName
            self.titleLabel.text = viewModel.symbol
            self.amountTextField.text = viewModel.amount
            
            guard let text = self.amountTextField.text else { return }
            let textRange = NSMakeRange(0, self.amountTextField.text?.count ?? 10)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: textRange)
            self.amountTextField.attributedText = attributedText

        }
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        self.contentView.addSubview(self.horizontalStackView)
        
        self.horizontalStackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: Constants.Margin).isActive = true
        self.horizontalStackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -Constants.Margin).isActive = true
        self.horizontalStackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: Constants.Margin).isActive = true
        self.horizontalStackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -Constants.Margin).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        self.setBorderAndShadow()
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }

    override func becomeFirstResponder() -> Bool {
        return amountTextField.becomeFirstResponder()
    }

    // MARK: - Private
    
    private func setBorderAndShadow() {
        self.contentView.layer.cornerRadius = Constants.CorneRadius
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.contentView.layer.masksToBounds = true
    }
}

//
//  CurrenciesListRouter.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit

class CurrenciesListRouter: CurrenciesListWireframe {
    var viewController: UIViewController?
    
    static func start() -> UIViewController {
        let view = CurrenciesListViewController()
        let presenter = CurrenciesListPresenter()
        let interactor = CurrenciesListInteractor()
        let router = CurrenciesListRouter()
        
        let navigation = UINavigationController(rootViewController: view)
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.viewController = view
        
        return navigation
    }
}

//
//  Client.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation

public enum Result<T> {
    case success(T)
    case error(Error)
}

class Client: NSObject {
    
    // MARK: - Types
    
    enum Route: String {
        case latest = "latest"
    }
    
    typealias UrlQuery = [String: Any]
    
    public enum APIError: Error {
        case network
        case parsing
    }
    
    private struct Constants {
        static let baseUrl = URL(string: "https://revolut.duckdns.org/")!
    }
    
    // MARK: - Methods
    
    func performRequest<T: Decodable>(path: String, parameters: [String: Any]?, completion: @escaping (_ result: Result<T>) -> Void) {
        
        guard let url = buildUrl(path: path, parameters: parameters) else {
            return
        }
        
        let urlRequest = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            // Checks for errors
            if let errorResult: Result<T> = self.errorResult(data: data, response: response, error: error) {
                completion(errorResult)
                return
            }
            
            // Decode data to corresponding Model
            do {
                guard let data = data else { throw APIError.parsing }
                
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let decodedData = try decoder.decode(T.self, from: data)
                
                completion(.success(decodedData))
            } catch {
                completion(.error(APIError.parsing))
                return
            }
        }
        
        task.resume()
    }
    
    // MARK: - Private
    
    private func errorResult<T>(data: Data?, response: URLResponse?, error: Error?) -> Result<T>? {
        // Checks for fundamental networking errors
        guard let data = data, error == nil else {
            if let errorMessage = error?.localizedDescription {
                print(errorMessage)
            }
            return .error(APIError.network)
        }
        
        // Checks for HTTP errors
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("Response error:\(String(describing: String(data: data, encoding: .utf8)))")
            return .error(APIError.network)
        }
        
        return nil
    }
    
    private func buildUrl(path: String, parameters: [String: Any]?) -> URL? {
        let url = Constants.baseUrl.appendingPathComponent(path)
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        
        // Encode url parameters into url
        if let parameters = parameters {
            urlComponents?.queryItems = parameters.map {
                URLQueryItem(name: $0.key, value: $0.value as? String)
            }
        }
        
        return urlComponents?.url
    }
}

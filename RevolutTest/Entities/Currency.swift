//
//  Currency.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation

enum Currency: String, Codable {
    case aud = "AUD"
    case bgn = "BGN"
    case brk = "BRL"
    case cad = "CAD"
    case chf = "CHF"
    case cny = "CNY"
    case czk = "CZK"
    case dkk = "DKK"
    case eur = "EUR"
    case gbp = "GBP"
    case hkd = "HKD"
    case hrk = "HRK"
    case huf = "HUF"
    case idr = "IDR"
    case ils = "ILS"
    case inr = "INR"
    case isk = "ISK"
    case jpy = "JPY"
    case krw = "KRW"
    case mxn = "MXN"
    case myr = "MYR"
    case nok = "NOK"
    case nzd = "NZD"
    case php = "PHP"
    case pln = "PLN"
    case ron = "RON"
    case rub = "RUB"
    case sek = "SEK"
    case sgd = "SGD"
    case thb = "THB"
    case ltry = "TRY"
    case usd = "USD"
    case zar = "ZAR"

    var name: String {
        switch self {
        case .aud:
            return "Australian Dollars"
        case .bgn:
            return "Lev"
        case .brk:
            return "Brazil"
        case .cad:
            return "Canadian Dollar"
        case .chf:
            return "Swiss Franc"
        case .cny:
            return "Yuan Renminbi"
        case .czk:
            return "Koruna"
        case .dkk:
            return "Danish Krone"
        case .eur:
            return "Euro"
        case .gbp:
            return "Manx pound"
        case .hkd:
            return "HKD"
        case .hrk:
            return "Croatian Dinar"
        case .huf:
            return "Forint"
        case .idr:
            return "Indonesian Rupiah"
        case .ils:
            return "Shekel"
        case .inr:
            return "Indian Rupee"
        case .isk:
            return "Icelandic Krona"
        case .jpy:
            return "Japanese Yen"
        case .krw:
            return "Won"
        case .mxn:
            return "Peso"
        case .myr:
            return "Ringgit"
        case .nok:
            return "Norwegian Krone"
        case .nzd:
            return "New Zealand Dollars"
        case .php:
            return "Peso"
        case .pln:
            return "Zloty"
        case .ron:
            return "Leu"
        case .rub:
            return "Ruble"
        case .sek:
            return "Krona"
        case .sgd:
            return "Dollar"
        case .thb:
            return "Baht"
        case .ltry:
            return "Lira"
        case .usd:
            return "US Dollar"
        case .zar:
            return "Rand"
        }
    }

    var flag: String {
        switch self {
        case .aud:
            return "🇦🇺"
        case .bgn:
            return "🇧🇬"
        case .brk:
            return "🇧🇷"
        case .cad:
            return "🇨🇦"
        case .chf:
            return "🇨🇭"
        case .cny:
            return "🇨🇳"
        case .czk:
            return "🇨🇿"
        case .dkk:
            return "🇩🇰"
        case .eur:
            return "🇪🇺"
        case .gbp:
            return "🇬🇧"
        case .hkd:
            return "🇭🇰"
        case .hrk:
            return "🇭🇷"
        case .huf:
            return "🇭🇺"
        case .idr:
            return "🇮🇩"
        case .ils:
            return "🇮🇱"
        case .inr:
            return "🇮🇳"
        case .isk:
            return "🇮🇸"
        case .jpy:
            return "🇯🇵"
        case .krw:
            return "🇰🇷"
        case .mxn:
            return "🇲🇽"
        case .myr:
            return "🇲🇾"
        case .nok:
            return "🇳🇴"
        case .nzd:
            return "🇳🇿"
        case .php:
            return "🇵🇭"
        case .pln:
            return "🇵🇱"
        case .ron:
            return "🇷🇴"
        case .rub:
            return "🇷🇺"
        case .sek:
            return "🇸🇪"
        case .sgd:
            return "🇸🇬"
        case .thb:
            return "🇹🇭"
        case .ltry:
            return "🇹🇷"
        case .usd:
            return "🇺🇸"
        case .zar:
            return "🇿🇦"
        }
    }
}

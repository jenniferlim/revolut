//
//  CurrenciesListPresenter.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation

class CurrenciesListPresenter: CurrenciesListPresentation {

	private struct Constants {
		static let ThresholdDecimal: Int = 2
		static let Overflow: Int = 9
		static let FirstIndexPath = IndexPath(row: 0, section: 0)
	}

    enum RatesState {
        case loaded
        case error
        case loading
    }

    var baseCurrencyAmount: Double = 0.0
    var baseCurrency: Currency = .eur

    var currenciesRate: CurrenciesRate? {
        didSet {
            DispatchQueue.main.async {
                self.reload()
            }
        }
    }

    var ratesState: RatesState = .loading

    var view: CurrenciesListView?
    
    var interactor: CurrenciesListInteractor!
    
    var router: CurrenciesListWireframe!
    
    func viewDidLoad() {
        self.fetchRates(for: .eur)
    }

    func didSelectItem(at indexPath: IndexPath, with currencyAmount: String) {

        guard var currenciesRate = currenciesRate, indexPath.section == 1 else { return }

        let index = indexPath.row
        let currency = currenciesRate.rates[index].currency

        self.fetchRates(for: currency)

        currenciesRate.rates.remove(at: index)
        currenciesRate.rates.insert(CurrencyRate(currency: self.baseCurrency, rate: 0.0), at: index)

        self.baseCurrency = currency
        self.baseCurrencyAmount = Double(currencyAmount) ?? 0.0

        let firstIndexPath = Constants.FirstIndexPath
        view?.moveItem(from: indexPath, to: firstIndexPath, with: [])
        view?.textFieldShouldBecomeFirstResponder(at: firstIndexPath)
    }

    func didChange(baseCurrencyAmount: String) {
        let amount = Double(baseCurrencyAmount) ?? 0.0

        self.baseCurrencyAmount = amount

        switch ratesState {
        case .loaded, .loading:
            self.reload()
        case .error:
            fetchRates(for: baseCurrency)
        }
    }

    // MARK: - Private

    private func fetchRates(for currency: Currency) {
        ratesState = .loading
        self.interactor.fetchLatestCurrencies(currency: currency.rawValue)
    }

    private func reload() {
        guard let currenciesRate = self.currenciesRate else { return }

        let baseCurrencyViewModel = CurrencyCellViewModel(currency: baseCurrency, value: baseCurrencyAmount)
        let viewModels: [CurrencyCellViewModel] = currenciesRate.rates.map {

            let value = ratesState == .loaded ? baseCurrencyAmount * $0.rate : 0
            return CurrencyCellViewModel(currency: $0.currency, value: value)
        }
        view?.reload(currencyViewModels: viewModels, baseCurrencyViewModel: baseCurrencyViewModel)
    }

	func textIsValid(_ text: String) -> Bool {

		// Restrict non-numeric characters (except for decimals and backspaces)
		if Double(text) == nil && text.count != 0 {
			return false
		}

		// Restrict ridiculous amounts, defensively prevent overflows
		if text.count > Constants.Overflow {
			return false
		}

		let containsDecimal = text.contains { $0 == "." }
		if containsDecimal {
			let components = text.split { return $0 == "." }

			// Only allow two digits after an initial decimal with no leading zero
			if components.count == 1 && components.first!.count > Constants.ThresholdDecimal && text.first == "." {
				return false
			}

			// 2 components are dollars and cents
			// Prevent more than 2 decimal digits from being entered
			if let cents = components.last, components.count == Constants.ThresholdDecimal && cents.count > Constants.ThresholdDecimal {
				return false
			}
		}

		return true
	}
}

extension CurrenciesListPresenter: CurrenciesListInteractorOutput {
    func currenciesRateFetched(currenciesRate: CurrenciesRate) {
        ratesState = .loaded
        self.currenciesRate = currenciesRate
    }
    
    func currenciesRateFetchFailed() {
        ratesState = .error
    }
}

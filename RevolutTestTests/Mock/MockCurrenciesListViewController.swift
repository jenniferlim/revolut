//
//  MockCurrenciesListViewController.swift
//  RevolutTestTests
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit
@testable import RevolutTest

class MockCurrenciesListViewController: CurrenciesListViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func reload(currencyViewModels: [CurrencyCellViewModel], baseCurrencyViewModel: CurrencyCellViewModel) {
		super.reload(currencyViewModels: currencyViewModels, baseCurrencyViewModel: baseCurrencyViewModel)
	}

	override func moveItem(from: IndexPath, to: IndexPath, with viewModels: [CurrencyCellViewModel]) {
		// Do nothing
	}

	override func textFieldShouldBecomeFirstResponder(at indexPath: IndexPath) {
		// Do nothing
	}

	override func reloadSections(_ sections: IndexSet) {
		// Do nothing
	}
}

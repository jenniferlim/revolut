//
//  AppRootContract.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit

protocol AppRootWireframe: class {
    func start(in window: UIWindow)
}

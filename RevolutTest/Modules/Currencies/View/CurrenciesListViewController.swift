//
//  CurrenciesListViewController.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit

class CurrenciesListViewController: UIViewController, CurrenciesListView {

    private struct Constants {
        static let Cell = "Cell"
        static let Margin: CGFloat = 16.0
		static let Insets: UIEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
		static let FirstIndexPath = IndexPath(row: 0, section: 0)
		static let CellHeight: CGFloat = 60.0
    }
    
    // MARK: - Properties

    var baseCurrencyViewModel: CurrencyCellViewModel = CurrencyCellViewModel(currency: .eur)
    var dataSource: [CurrencyCellViewModel] = []
    
    let collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = Constants.Insets
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()

    var presenter: CurrenciesListPresentation!

    // MARK: Override

    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter.viewDidLoad()
        
        self.collectionView.register(CurrencyCell.self, forCellWithReuseIdentifier: Constants.Cell)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.view.addSubview(self.collectionView)
        
        self.collectionView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: Constants.Margin).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -Constants.Margin).isActive = true
        self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: Constants.Margin).isActive = true
        self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -Constants.Margin).isActive = true

        self.view.backgroundColor = .white
        self.collectionView.backgroundColor = .white

        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

	// MARK: - CurrenciesListView

	func reload(currencyViewModels: [CurrencyCellViewModel], baseCurrencyViewModel: CurrencyCellViewModel) {

		if baseCurrencyViewModel != self.baseCurrencyViewModel, let cell = collectionView.cellForItem(at: Constants.FirstIndexPath) as? CurrencyCell {

			cell.viewModel = baseCurrencyViewModel
			cell.amountTextField.delegate = self
			cell.amountTextField.isEditable = true

			self.baseCurrencyViewModel = baseCurrencyViewModel
		}

		self.dataSource = currencyViewModels

		self.reloadSections(IndexSet(arrayLiteral: 1))
	}

	func reloadSections(_ sections: IndexSet) {
		DispatchQueue.main.async {
			self.collectionView.reloadSections(sections)
		}
	}

	func moveItem(from: IndexPath, to: IndexPath, with viewModels: [CurrencyCellViewModel]) {

		collectionView.performBatchUpdates({
			collectionView.moveItem(at: to, to: IndexPath(row: 0, section: 1))
			collectionView.moveItem(at: from, to: to)
		}, completion: nil)
	}

	func textFieldShouldBecomeFirstResponder(at indexPath: IndexPath) {
		guard let cell = collectionView.cellForItem(at: indexPath) as? CurrencyCell else {
			return
		}
		cell.amountTextField.delegate = self
		_ = cell.becomeFirstResponder()
	}
}

extension CurrenciesListViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return self.dataSource.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cell, for: indexPath) as? CurrencyCell else {
            fatalError("Unable to dequeue Cell")
        }

        if indexPath.section == 0 {
            cell.viewModel = self.baseCurrencyViewModel
            cell.amountTextField.delegate = self
            cell.amountTextField.isEditable = true
        } else {
            cell.viewModel = self.dataSource[indexPath.row]
            cell.amountTextField.delegate = nil
            cell.amountTextField.isEditable = false
        }
        return cell
    }
}

extension CurrenciesListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard
            let cell = collectionView.cellForItem(at: indexPath) as? CurrencyCell,
            let amountText = cell.amountTextField.text else {
            return
        }

        self.presenter.didSelectItem(at: indexPath, with: amountText)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.bounds.width, height: Constants.CellHeight)
    }
}

extension CurrenciesListViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

		guard
			let text = textField.text,
			let textRange = Range(range, in: text) else { return false }

		let replacementText = text.replacingCharacters(in: textRange,
													   with: string)

		if self.presenter.textIsValid(replacementText) == false {
			return false
		}

        self.presenter.didChange(baseCurrencyAmount: replacementText)

        return true
    }
}

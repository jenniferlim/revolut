//
//  Mocks.swift
//  RevolutTestTests
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import Foundation

public func json<T: Decodable>(filename: String, bundle: Bundle = Bundle.revolutTests) -> T {
	guard
		let path = bundle.path(forResource: filename, ofType: "json"),
		let data = NSData(contentsOfFile: path) else {
			fatalError("Could not load json mock file: \(filename)")
	}

	let decoder = JSONDecoder()
	decoder.dateDecodingStrategy = .iso8601

	// Force unwrap only because it is used in test target only.
	let decodedData = try! decoder.decode(T.self, from: data as Data)

	return decodedData
}

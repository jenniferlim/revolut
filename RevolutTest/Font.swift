//
//  Font.swift
//  RevolutTest
//
//  Created by Jennifer lim on 12/8/18.
//  Copyright © 2018 Jennifer lim. All rights reserved.
//

import UIKit

struct Font {
	static let AmountText = UIFont.systemFont(ofSize: 24, weight: .semibold)
	static let Title = UIFont.systemFont(ofSize: 15, weight: .bold)
	static let Flag = UIFont.systemFont(ofSize: 48, weight: .regular)
	static let Text = UIFont.systemFont(ofSize: 12, weight: .semibold)
}
